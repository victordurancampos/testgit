const bcrypt = require('bcryptjs');
const jwt =require('jsonwebtoken');
var Usuario=require('../../models/usuario');

module.exports={
    authenticate:function(req,res,next){
        Usuario.findOne({email: require.body.email},function(err,userInfo){
            if(err){
                next(err);
            } else {
                if(userInfo==null){  return res.status(401).json({status:'error',message:'Usuario invalido'});}
                if(userInfo!=null && bcrypt.compareSync(req.body.password,userInfo.password)){
                    userInfo.save(function (err,Usuario){
                        const token = jwt.sign({id: userInfo._id},req.app.get('secretKey'),{expiresIn:'7d'});
                        res.status(200).json({message:'Usuario encontrado',data:{usuario:Usuario,token:token}});
                    });
                } else {
                    res.status(401).json({status:'error', message:'Usuario invalido'});
                }
            }
        });
     },

     forgotPassword: function(req,res,next){
        Usuario.findOne({email: require.body.email},function(err,userInfo){
            if(!Usuario) return res.status(401).json({status:'error',message:'No existe el usuario',data:null});
            Usuario.resetPassword(function(err){
                if(err){return  next(err);}
                res.status(200).json({message:'Se envio un email para restablecer password',data:null});
            });
        });
     },
}


