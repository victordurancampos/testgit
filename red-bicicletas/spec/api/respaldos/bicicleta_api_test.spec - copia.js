var Bicicleta = require('../../models/bicicletas');
var Server = require('../../bin/www');
var request = require('request');


beforeEach(() => { console.log('testeando...'); Bicicleta.allBicis=[]; })


describe('Bicicleta API', () => {
    describe('GET BICICLETAS /', () => {
        it('Status 200', () => {
            expect(Bicicleta.allBicis.length).toBe(0);
            var a= new Bicicleta(1,'rojo','urbana',[19.336006, -99.189846]);
            Bicicleta.add(a);
                request.get('http://localhost:5000/api/bicicletas', function(error, response, body) {
                expect(response.statusCode).toBe(200);
            });

        });   
 
    });
});


describe('POST BICICLETAS /create', () => {
    it('Status 200', (done) => {
        var headers = { 'content-type': 'application/json' };
        var abici= new Bicicleta(4,'Azul','urbana',[19.336006, -99.189846]);
        Bicicleta.add(abici);
       
        request.post({
            headers: headers,
                url: 'http://localhost:5000/api/bicicletas/create',
                body: abici,
                json: true
            },
            function(error, response, body) {
                console.log("error: " + error);
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(4).color).toBe('Azul');
                done();
            });
    });
});





    describe('POST BICICLETAS /update', () => {
        it('Status 200', (done) => {
            var headers = { 'content-type': 'application/json' };
            var abici= new Bicicleta(10,'Rojo','urbana',[19.336006, -99.189846]);
            Bicicleta.add(abici);

            request.post({
                    headers: headers,
                    url: 'http://localhost:5000/api/bicicletas/update',
                    body: abici,
                    json: true
                },
                function(error, response, body) {
                    console.log("error: " + error);
                    expect(response.statusCode).toBe(404);
                    expect(Bicicleta.findById(10).color).toBe('Rojo');
                    done();
                });
        });
    });


    describe('POST BICICLETAS /delete', () => {
        it('Status 200', (done) => {
            expect(Bicicleta.allBicis.length).toBe(0);
            var headers = { 'content-type': 'application/json' };
            var abici= new Bicicleta(10,'Rojo','urbana',[19.336006, -99.189846]);
            Bicicleta.add(abici);


            expect(Bicicleta.allBicis.length).toBe(1);
            request.post({
                    headers: headers,
                    url: 'http://localhost:5000/api/bicicletas/delete',
                    body: abici,
                    json: true
                },
                function(error, response, body) {
                    console.log("error: " + error);
                    expect(response.statusCode).toBe(404);
                    expect(Bicicleta.allBicis.length).toBe(1);
                    done();
                });
        });
    });

