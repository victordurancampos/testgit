var mongoose =require('mongoose');
var Bicicleta = require('../../models/bicicletas');
var Server = require('../../bin/www');
var request = require('request');

var base_url="http://localhost:5000/api/bicicletas";


describe('Bicicleta API', () => {
    beforeEach(function(done) {
        var mongoDB = 'mongodb://localhost/red-bicicletas';
        mongoose.connect(mongoDB, { useNewUrlParser: true });
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'MongoDB errror de conexion'));
        db.once('open', function() {
            console.log('Conectado a la base de datos MongoDB');
            done();
        });
    });

    
    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(error, success) {
            if (error) {
                console.error(error);
            }
            done();
        });
    });

    
    describe('GET BICICLETAS /', () => {
        it('Status 200', (done) => {
            request.get(base_url, function(error, response, body) {
                var resultado= JSON.parse[body];
                expect(response.statusCode).toBe(200);
                expect(resultado.Bicicleta.length).toBe(0);
                done();
            });
        });   
    });
    
    
    describe('POST BICICLETAS /create', () => {
        it('Status 200', (done) => {

            var headers = { 'content-type': 'application/json' };
            var abici='{"code":10,"color":"rojo","modelo":"montana","lat":55.45,"lng":40}';

            request.post({
                headers: headers,
                    url: base_url+'/create',
                    body: abici,
                    json: true
                },
                function(error, response, body) {
                    expect(response.statusCode).toBe(200);
                    var bici=JSON.parse(body).Bicicleta;
                    console.log(bici);
                    expect(bici.color).toBe('rojo');
                    expect(bici.ubicacion[0]).toBe(-34);
                    expect(bici.ubicacion[1]).toBe(-56);
                    done();
                });
        });
    });



    describe('POST BICICLETAS /delete', () => {
        it('Status 204', (done) => {

            var headers = { 'content-type': 'application/json' };
            var abici='{"code":10,"color":"rojo","modelo":"montana","lat":55.45,"lng":40}';

            request.post({
                    headers: headers,
                    url: base_url+'/delete',
                    body: abici,
                    json: true
                },
                function(error, response, body) {
                    //expect(response.statusCode).toBe(204);
                    //expect(Bicicleta.allBicis.length).toBe(1);
                    //console.log(bici);
                    done();
                });
        });
    });


});   





/*
beforeEach(() => { console.log('testeando...'); Bicicleta.allBicis=[]; })


describe('Bicicleta API', () => {
    describe('GET BICICLETAS /', () => {
        it('Status 200', () => {
            expect(Bicicleta.allBicis.length).toBe(0);
            var a= new Bicicleta(1,'rojo','urbana',[19.336006, -99.189846]);
            Bicicleta.add(a);
                request.get('http://localhost:3000/api/bicicletas', function(error, response, body) {
                expect(response.statusCode).toBe(200);
            });

        });   
 
    });
});


describe('POST BICICLETAS /create', () => {
    it('Status 200', (done) => {
        var headers = { 'content-type': 'application/json' };
        var abici= new Bicicleta(4,'Azul','urbana',[19.336006, -99.189846]);
        Bicicleta.add(abici);
       
        request.post({
            headers: headers,
                url: 'http://localhost:3000/api/bicicletas/create',
                body: abici,
                json: true
            },
            function(error, response, body) {
                console.log("error: " + error);
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(4).color).toBe('Azul');
                done();
            });
    });
});





    describe('POST BICICLETAS /update', () => {
        it('Status 200', (done) => {
            var headers = { 'content-type': 'application/json' };
            var abici= new Bicicleta(10,'Rojo','urbana',[19.336006, -99.189846]);
            Bicicleta.add(abici);

            request.post({
                    headers: headers,
                    url: 'http://localhost:3000/api/bicicletas/update',
                    body: abici,
                    json: true
                },
                function(error, response, body) {
                    console.log("error: " + error);
                    expect(response.statusCode).toBe(404);
                    expect(Bicicleta.findById(10).color).toBe('Rojo');
                    done();
                });
        });
    });


    describe('POST BICICLETAS /delete', () => {
        it('Status 200', (done) => {
            expect(Bicicleta.allBicis.length).toBe(0);
            var headers = { 'content-type': 'application/json' };
            var abici= new Bicicleta(10,'Rojo','urbana',[19.336006, -99.189846]);
            Bicicleta.add(abici);


            expect(Bicicleta.allBicis.length).toBe(1);
            request.post({
                    headers: headers,
                    url: 'http://localhost:3000/api/bicicletas/delete',
                    body: abici,
                    json: true
                },
                function(error, response, body) {
                    console.log("error: " + error);
                    expect(response.statusCode).toBe(404);
                    expect(Bicicleta.allBicis.length).toBe(1);
                    done();
                });
        });
    });


*/