var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicletas');
var Usuario = require('../../models/usuario');
var Reserva = require('../../models/reserva');


describe('Testeando Usuarios', function() {
    beforeEach(function(done) {
        var mongoDB = 'mongodb://localhost/red-bicicletas';
        mongoose.connect(mongoDB, { useNewUrlParser: true });
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'MongoDB errror de conexion'));
        db.once('open', function() {
            console.log('Conectado a la base de datos MongoDB');
            done();
        });
    });

    afterEach(function(done) {
        Reserva.deleteMany({}, function(err, success) {
            if (err)  console.error(err);
            Usuario.deleteMany({},function(err, success) {
                if (err)  console.error(err);
                Bicicleta.deleteMany({},function(err, success) {
                    if (err)  console.error(err);
                    done();   
                });           
            });    
           
        });
    });

    describe('Cuando un usuario reserva una bici', function() {
        it ('Desde la reserva',(done) => {
            const usuario= new Usuario({nombre:"Ezequiel"});
            usuario.save();
            const bicicleta= new Bicicleta({code:1,color:'rojo',modelo:'Urbana'});
            bicicleta.save();
            var hoy = new Date();
            var manana=new Date();
            manana.setDate(hoy.getDate()+1);
            usuario.reservar(bicicleta.id,hoy,manana, function(err,reserva){
                Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err,reservas){
                    console.log(reservas[0]);
                    expect(reservas.length).toBe(1);
                    expect(reservas[0].diasDeReserva()).toBe(2);
                    expect(reservas[0].bicicleta.code).toBe(1);
                    expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
                    done();
                });
                
            });

        });
    });    


});  


    






