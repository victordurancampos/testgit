var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicletas');


describe('Testeando Bicicletas', function() {
    beforeEach(function(done) {
        var mongoDB = 'mongodb://localhost/red-bicicletas';
        mongoose.connect(mongoDB, { useNewUrlParser: true });
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'MongoDB errror de conexion'));
        db.once('open', function() {
            console.log('Conectado a la base de datos MongoDB');
            done();
        });
    });

    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(error, success) {
            if (error) {
                console.error(error);
            }
            done();
        });
    });

    describe('Bicicleta.createInstance', () => {
        it('Crea una instancia de Bicicleta', () => {
            var bici = Bicicleta.createInstance(1, "Verde", "Urbana", [-34.5, -54.1]);
            expect(bici.code).toBe(1);
            expect(bici.color).toBe("Verde");
            expect(bici.modelo).toBe("Urbana");
            expect(bici.ubicacion[0]).toBe(-34.5);
            expect(bici.ubicacion[1]).toBe(-54.1);
        });
    });


    describe('Bicicletas.allBicis', () => {
        it('Comienza vacia', (done) => {
            Bicicleta.allBicis(function(err, bicis) {
                expect(bicis.length).toBe(0);
                done();
            })
        });
    });


    describe('Bicicletas.add', () => {
        it('Agrega solo una bicicleta', (done) => {
            var bici = new Bicicleta({ code: 1, color: "Verde", modelo: "Urbana" });
            Bicicleta.add(bici, function(err, newBici) {
                if (err) console.log(err);
                    Bicicleta.allBicis(function(err, bicis) {
                    expect(bicis.length).toBe(1);
                    expect(bicis[0].code).toBe(bici.code);
                    done();
                });
            });
        });
    });
    

    describe('Bicicletas.findByCode', () => {
        it('Devuelve la bicicleta con codigo 1', (done) => {
            var bici = new Bicicleta({ code: 1, color: "Verde", modelo: "Urbana" });
            Bicicleta.add(bici, function(err, newBici) {
                if (err) console.log(err);
                    var bici2 = new Bicicleta({ code: 1, color: "Verde", modelo: "Urbana" });
                    Bicicleta.add(bici2, function(err, newBici) {
                    if (err) console.log(err);
                        Bicicleta.findByCode(1, function(error, targetBici) {
                        expect(targetBici.code).toBe(bici.code);
                        expect(targetBici.color).toBe(bici.color);
                        expect(targetBici.modelo).toBe(bici.modelo);
                        done();
                    });
                });
            });
        });
    });
    

});










/*
beforeEach( () => { Bicicleta.allBicis=[] });


describe('Bicicleta.allBicis',() => {
    it('Comienza vacia', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});


describe('Bicicleta.add',() => {
    it('Agregamos un elemento', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        var a= new Bicicleta(1,'rojo','urbana',[19.336006, -99.189846]);
        Bicicleta.add(a);
        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);

    });
});



describe('Bicicleta.findById',() => {
    it('Debe devolver la bici con ID igual a 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        var abici1= new Bicicleta(1,'rojo','urbana',[19.336006, -99.189846]);
        var abici2= new Bicicleta(2,'blanca','pista',[19.330518, -99.187104]);

        Bicicleta.add(abici1);
        Bicicleta.add(abici2);

        var targetBici=Bicicleta.findById(1);
        
        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(abici1.color);
        expect(targetBici.modelo).toBe(abici1.modelo);
    });
});
*/