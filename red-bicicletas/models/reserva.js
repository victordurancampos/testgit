var mongose = require('mongoose');
var moment = require('moment');
var Schema = mongose.Schema;

var reservaSchema = new Schema ({
    desde: Date,
    hasta: Date,
    bicicleta:{type:mongose.Schema.Types.ObjectId,ref:'Bicicleta'},
    usuario:{type:mongose.Schema.Types.ObjectId,ref:'Usuario'},
});

reservaSchema.methods.diasDeReserva=function(){
   //return moment(this.hasta),diff(moment(this.desde),'days')+1;
   return moment(this.hasta).diff(moment(this.desde),'days')+1;
};


module.exports=mongose.model('Reserva',reservaSchema);