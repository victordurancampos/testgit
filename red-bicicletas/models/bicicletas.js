var Mongoose = require('mongoose');
var Schema = Mongoose.Schema;

var BicicletaSchema = new Schema({
    code: Number,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number],
        index: { type: '2dsphere', sparse: true }
    }
});


BicicletaSchema.statics.createInstance = function(code, color, modelo, ubicacion) {
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });
};


BicicletaSchema.methods.toString = function() {
    return 'code: ' + this.code + ' | Color: ' + this.color;
};

BicicletaSchema.statics.allBicis = function(cb) {
    return this.find({}, cb);
};

BicicletaSchema.statics.add = function(bici, cb) {
    return this.create(bici, cb);
};

BicicletaSchema.statics.findByCode = function(code, cb) {
    return this.findOne({ code: code }, cb);
};

BicicletaSchema.statics.removeByCode = function(code, cb) {
    return this.deleteOne(code, cb);
};




module.exports = Mongoose.model('Bicicleta', BicicletaSchema);


/*
var Bicicleta = function(id,color,modelo,ubicacion){
    this.id=id;
    this.color=color;
    this.modelo=modelo;
    this.ubicacion=ubicacion;
}

Bicicleta.prototype.toString=function(){
    return 'id: ' + this.id + ' | color: ' + this.color;
}

Bicicleta.allBicis=[];

Bicicleta.add= function(aBici){
     Bicicleta.allBicis.push(aBici);
}

//var a= new Bicicleta(1,'rojo','urbana',[19.336006, -99.189846]);
//var b= new Bicicleta(2,'blanca','pista',[19.330518, -99.187104]);
//var c= new Bicicleta(3,'azul','montana',[19.327524, -99.194529]);


//Bicicleta.add(a);
//Bicicleta.add(b);
//Bicicleta.add(c);


Bicicleta.findById=function(aBicId){
    var aBici=Bicicleta.allBicis.find(x=> x.id==aBicId);
    if(aBici)
       return aBici;
    else
       throw new Error(`No existe una bicicleta con el Id: ${aBicId}`);   
}

Bicicleta.removeID=function(aBicId){
    //var aBici = Bicicleta.findById(aBicId);
    for (i=0; i<=Bicicleta.allBicis.length;i++){
       if(Bicicleta.allBicis[i].id==aBicId){
           Bicicleta.allBicis.splice(i,1);
           break;
       }
    }      

}

Bicicleta.rem=function(aBicId){
    //var aBici = Bicicleta.findById(aBicId);
    for (i=0; i<=Bicicleta.allBicis.length;i++){
       if(Bicicleta.allBicis[i].id==aBicId){
           Bicicleta.allBicis.splice(i,1);
           break;
       }
    }      

}



module.exports=Bicicleta;

*/