var mongoose = require('mongoose');
var Reserva = require('./reserva');
//const bcrypt=require('bcrypt');
const bcrypt = require('bcryptjs');
const saltRounds=10;
var Schema = mongoose.Schema;


var uniqueValidator = require('mongoose-unique-validator');
var Token = require('../models/token');
var crypto = require('crypto');
var mailer = require('../mailer/mailer');


const validateEmail=function(email){
    const re=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(email);
}

var usuarioSchema = new Schema({
    nombre:{
        type: String,
        trim: true,
        required:[true,'El nombre es requerido']
    },
    email:{
        type: String,
        trim: true,
        required:[true,'El email es requerido'],
        lowercase:true,
        unique:true,
        validate:[validateEmail,'Por favor ingreso un Email valido'],
        match:[/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/]
    },
    password:{
        type: String,
        trim: true,
        required:[true,'El password es requerido']

    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado:{
        type: Boolean,
        default:false
    }
});


usuarioSchema.plugin(uniqueValidator, { message: 'El {PATH} ya existe con otro usuario' });


usuarioSchema.pre('save',function(next){
    if (this.isModified('password')){
        this.password=bcrypt.hashSync(this.password,saltRounds);
    }
    next();
});

usuarioSchema.methods.validatePassword=function(password){
    return bcrypt.compareSync(password,this.password);
}

usuarioSchema.methods.reservar=function(biciID,desde,hasta,cb){
    var reserva=new Reserva({usuario: this._id, bicicleta: biciID, desde: desde, hasta: hasta});
    console.log(reserva);
    reserva.save(cb);
}



usuarioSchema.methods.enviar_email_bienvenida = function(cb) {
    const token = new Token({ _userId: this.id, token: crypto.randomBytes(16).toString('hex') });
    const email_destino = this.email;
    token.save(function(err) {
        if (err) { return console.log(err.message); }

        const emailOptions = {
            from: 'no-reply@redbicicletas.com',
            to: email_destino,
            subject: 'Verificación de cuenta',
            text: 'Hola, por favor para verificar su cuenta haga click en el siguiente enlance:' + 'http://localhost:5000' + '\/token/confirmation\/' + token.token
        };

        mailer.sendMail(emailOptions, function(err) {
            if (err) { return console.log(err.message); }
            console.log('A verification email has benn sent to: ' + email_destino);
        });
    });
}

usuarioSchema.methods.resetPassword=function (cb){
    const token = new Token({_userId:this.id, token: crypto.pseudoRandomBytes(16).toString('hex')});
    const email_destino=this.email;
    token.save(function(err){
        const mailOptions={
            from:'no_replay@red-bicicletas.com',
            to: email_destino,
            subject: 'Reseteo de Password de cuenta',
            text: 'hola'
        }

        mailer.sendMail(mailOptions,function(err){
            if(err){return cb(err);}
            console.log('Se envio un email');
        });
        cb(null);
    });    
}



module.exports = mongoose.model('Usuario',usuarioSchema);