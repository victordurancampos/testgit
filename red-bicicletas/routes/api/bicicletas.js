var express =require('express');
var router=express.Router();

var bicicletaController=require('../../controllers/api/bibicletaControllerAPI');


router.get('/',bicicletaController.bicicleta_list);

router.post('/create',bicicletaController.Bicicleta_create);
router.delete('/delete',bicicletaController.Bicicleta_delete);


module.exports =router;
