var map = L.map('main_map').setView([19.330562, -99.187109], 13);

L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://cloudmade.com">CloudMade</a>',
maxZoom: 50
}).addTo(map);

L.control.scale().addTo(map);

/*
L.marker([19.336006, -99.189846],{draggable: true}).addTo(map);
L.marker([19.330518, -99.187104],{draggable: true}).addTo(map);
L.marker([19.327524, -99.194529],{draggable: true}).addTo(map);

*/


$.ajax({
    dataType: "json",
    url:"api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion,{title: bici.id}).addTo(map);
        });
    }
});


