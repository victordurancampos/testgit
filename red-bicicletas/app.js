var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var jwt=require('jsonwebtoken');

const passport=require('./config/passport');
const session=require('express-session');
const Usuario=require('./models/usuario');
const Token = require('./models/token');


var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var bicicletaRouter = require('./routes/bicicletas');
var bicicletasAPIRouter = require('./routes/api/bicicletas');
var usuarioAPIRouter = require('./routes/api/usuarios');
var usuariosRouter = require('./routes/usuarios');
var tokenRouter = require('./routes/token');
var authAPIRouter =require('./routes/api/auth');


const store = new session.MemoryStore;

var app =express();
app.use(session({
    cookie: { maxAge: 240*60*100},
    store: store,
    saveUninitialized: true,
    resave:'true',
    secret:'ponercualquiercosa'
}));

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/red_bicicletas', {useNewUrlParser: true});
mongoose.Promise=global.Promise;
var db = mongoose.connection;

db.on('error', console.error.bind(console, 'Mongo connection error:'));
db.once('open', function() {
  // we're connected!
});

var app =express();
app.set('secretKey','jwt_pwd_!!223344');


app.use( session({
    cookie:{maxAge: 240*60*60*100},
    store: store,
    saveUninitialized:true,
    resave: 'true',
    secret:'red_bicis'
}));




// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
//app.use('/users', usersRouter);
//app.use('/bicicletas',bicicletaRouter);
//app.use('/api/bicicletas',bicicletasAPIRouter);

app.use('/api/usuarios',usuarioAPIRouter);
app.use('/usuarios', usuariosRouter);
app.use('/token', tokenRouter);

app.use('/bicicletas',loggetIn,bicicletaRouter);
app.use('/api/bicicletas',validarUsuario,bicicletasAPIRouter);

app.use('/api/auth',authAPIRouter);




app.get('/login',function(req,res){
    res.render(session/login);
});


app.post('/login',function(req,res,next){
    passport.authenticate('local',function(err,usuario,info){
        if(err) return next(err);
        if(!usuario) return res.render('session/login',{info});
        req.login(usuario,function(err){
            if(err) return next(err);
            return res.redirect('/');
        });
    }) (req,res,next);
    
});


app.get('/logout',function(req,res){
  req.logout();
  res.redirect('/');
});



app.get('/forgotPassword',function(req,res){
  res.render('session/forgotPassword');
});


app.post('/forgotPassword',function(req,res){
  Usuario.findOne({ email: req.body.email }, function (err, usuario) {
    if (!usuario) return res.render('session/forgotPassword', {info:'info' , message: 'No existe el email para un usuario existente.'}); 
    
    usuario. resetPassword(function(err){ 
        if (err) return next(err);
        console.log('session/forgotPasswordMessage'); 
    });
    res.render('session/forgotPasswordMessage');      
  });

});


app.get('/resetPassword/:token', function(req,res, next){ 
  Token.findOne({ token: req.params.token }, function (err, token) { 
      if (!token) return res.status(400).send({ type: 'not-verified', msg: 'No existe un usuario asociado al token. Verifique que su token no haya expirado.' }); 
      Usuario.findId(token._userId, function(err,usuario){
          if(!usuario) return res.status(400).send({ msg: 'No existe usuario con ese Token'});
          res.render('session/ResetPassrord',{errors:{},usuario: usuario });
      });
  });
});  


app.post('/resetPassword/:token', function(req,res, next){ 
    if(req.body.password !=req.body.confirm_Password){
        res.render('session/resetPassword',{errors: {confirm_Password:{message:'no coincide el password ingresado'}},usuario: new Usuario({email:req.body.email})});
        return;
    }
    Usuario.findOne({email:req.body.email},function(){
        usuario.password=req.body.password;
        usuario.save(function(err){
              if(err) {
                   res.render('session/resetPassword',{errors:err.errors,usuario: new Usuario({email:req.body.email})});
              } else {
                  res.redirect('/login');
              }
        });
    });
}); 




// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


function loggetIn(req,res,next){
    if(req.usuario){
        next();
    } else {
      console.log('Usuario no logueado');
      res.redirect('/login');
    }
};


function validarUsuario(req,res,next){
   jwt.verify(req.headers['x-access-token'],req.app.get['secretKey'],function(err,decoded){
      if(err){
          res.json({status: 'error', message: err.message,data: null});
      } else {
          req.body.userId=decoded.id;
          console.log('Jwt verificado'+decoded);
          next();
      }
   });
};


module.exports = app;
