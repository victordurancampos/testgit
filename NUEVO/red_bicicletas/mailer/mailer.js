const nodemailer = require('nodemailer');
const sgTransport = require('nodemailer-sendgrid-transport');

/* const mailConfig = {
    host: 'smtp.ethereal.email',
    port: 587,
    auth: {
        user: 'titus.champlin@ethereal.email',
        pass: 'Y9HkdU7QV3fzQcqUFN'
    }
}; */

let mailConfig;
if (process.env.NODE_ENV === 'production') {
    const options = {
        auth: {
            api_key: process.env.SENGRID_API_SECRET
        }
    }
    mailConfig = sgTransport(options);
} else {
    if (process.env.NODE_ENV === 'staging') {
        console.log('XXXXXXXXXXX');
        const options = {
            auth: {
                api_key: process.env.SENGRID_API_SECRET
            }
        }
        mailConfig = sgTransport(options);
    } else {
        //Ethereal
        mailConfig = {
            host: 'smtp.ethereal.email',
            port: 587,
            auth: {
                user: 'titus.champlin@ethereal.email',
                pass: 'Y9HkdU7QV3fzQcqUFN'
            }
        };
    }
}

module.exports = nodemailer.createTransport(mailConfig);