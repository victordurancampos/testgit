var Bicicleta = require('../../modelo/bicicleta');
var mongoose = require('mongoose');

describe('Testing de Bicicletas', function () {
    beforeEach(function (done) {
        var mongoDB = 'mongodb://localhost/test';
        mongoose.connect(mongoDB, { useNewUrlParser: true });
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'Error al conectar'));
        db.once('open', function () {
            console.log('Conectado!');
            done();
        });
    });

    afterEach(function (done) {
        Bicicleta.deleteMany({}, function (err, success) {
            if (err) console.log(err);
            done();
        });
    });

    describe('Bicileta.crearInstancia', () => {
        it('crea una instancia de Bicicleta', () => {
            var bici = Bicicleta.crearInstancia(1, 'Morado', 'Urbana', [5.743343354, -73.11222353]);
            expect(bici.code).toBe(1);
            expect(bici.color).toBe('Morado');
            expect(bici.modelo).toBe('Urbana');
            expect(bici.ubicacion[0]).toBe(5.743343354);
            expect(bici.ubicacion[1]).toBe(-73.11222353);
        });
    });

    describe('Bicicleta.todas', () => {
        it('comienza vacia', (done) => {
            Bicicleta.todas((err, bicis) => {
                expect(err).toBeNull();
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.agregar', () => {
        it('agrega solo una bici', (done) => {
            const bici = Bicicleta.crearInstancia(1, 'Morado', 'Urbana', [5.743343354, -73.11222353]);
            Bicicleta.agregar(bici, (err, nueva) => {
                expect(err).toBeNull();
                Bicicleta.todas(function (err, bicis) {
                    expect(bicis.length).toBe(1);
                    expect(bicis[0].code).toBe(bici.code);
                    done();
                });
            });
        });
    });

    describe('Bicicleta.editar', () => {
        it('editar bici', (done) => {
            const bici = Bicicleta.crearInstancia(1, 'Morado', 'Urbana', []);
            Bicicleta.agregar(bici, (err, nueva) => {
                if (err) {
                    console.log(err);
                };
                const biciNueva = Bicicleta.crearInstancia(1, 'Morado', 'Urbana', [5.743343354, -73.11222353]);
                Bicicleta.editar(biciNueva, (err, nueva) => {
                    expect(err).toBeNull();
                    Bicicleta.todas(function (err, bicis) {
                        expect(err).toBeNull();
                        expect(bicis.length).toBe(1);
                        expect(bicis[0].code).toBe(biciNueva.code);
                        expect(bicis[0].ubicacion[0]).toBe(5.743343354);
                        done();
                    });
                });
            });
        });
    });
});