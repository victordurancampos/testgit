var Bicicleta = require('../../modelo/bicicleta');
var Usuario = require('../../modelo/usuario');
var Reserva = require('../../modelo/reserva');
var mongoose = require('mongoose');



describe('Testing Usuarios', function () {
    beforeEach(function (done) {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true });
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'Error conectando a la bd'));
        db.once('open', function () {
            console.log('Conexión correcta!');
            done();
        });
    });

    afterEach(function (done) {
        Reserva.deleteMany({}, function (err, success) {
            if (err) console.log(err);
            Usuario.deleteMany({}, function (err, success) {
                if (err) console.log(err);
                Bicicleta.deleteMany({}, function (err, success) {
                    if (err) console.log(err);
                    done();
                })
            });
        });
    });

    describe('Cuando un Usuario reserva una bici', () => {
        it('deberia existir la reserva', (done) => {
            const usuario = new Usuario({ nombre: 'Juan' });
            usuario.save();
            const bicicleta = new Bicicleta({ code: 1, color: "verde", modelo: "urbano" });
            bicicleta.save();
            var hoy = new Date();
            var mañana = new Date();
            mañana.setDate(hoy.getDate() + 1);
            usuario.reservar(bicicleta.id, hoy, mañana, (err, reserva) => {
                Reserva.find({}).populate('bicicleta').populate('usuario').exec(function (err, reservas) {
                    expect(reservas.length).toBe(1);
                    expect(reservas[0].diasDeReserva()).toBe(1);
                    expect(reservas[0].bicicleta.code).toBe(1);
                    expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
                    done();
                });
            });
        });
    });
});