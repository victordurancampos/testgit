var Bicicleta = require('../../../modelo/bicicleta');

exports.lista_bicicletas = (req, res) => {
    Bicicleta.todas((error, bicis) => {
        res.status(200).json({
            respuesta: bicis
        });
    });
}

exports.bicicletaPorId = (req, res) => {
    codigo = req.params.id;
    Bicicleta.encontrarPorCodigo(codigo, (error, bici) => {
        res.status(200).json({
            respuesta: bici
        });
    });
}

/**
 * Requiere un objeto bicicleta
 * Ejemplo 
 * {
 *   "bicicleta": {
 *      "id": 3,
 *       "color": "Morado",
 *       "modelo": "urbana",
 *       "ubicacion": [
 *           5.789438,
 *           -73.127921
 *       ]
 *   }
 * }
 */
exports.agregar_bicicleta = (req, res) => {
    var mensaje = '';
    const cuerpo = req.body.bicicleta;
    Bicicleta.encontrarPorCodigo(cuerpo.id, (error, bici) => {
        if (bici) {
            mensaje = `La bicicleta con codigo ${cuerpo.id} ya existe`;
        }
        else {
            const bici = Bicicleta.crearInstancia(cuerpo.id, cuerpo.color, cuerpo.modelo, cuerpo.ubicacion);
            Bicicleta.agregar(bici);
            mensaje = `La bicicleta fue agregada exitosamente`;
        }
        res.status(200).json({
            respuesta: mensaje
        });
    });
}

/**
 * Requiere un objeto bicicleta
 * Ejemplo 
 * {
 *   "bicicleta": {
 *      "id": 3,
 *       "color": "Morado",
 *       "modelo": "urbana",
 *       "ubicacion": [
 *           5.789438,
 *           -73.127921
 *       ]
 *   }
 * }
 */
exports.editar_bicicleta = (req, res) => {
    var mensaje = '';
    const cuerpo = req.body.bicicleta;
    Bicicleta.encontrarPorCodigo(cuerpo.id, (error, bici) => {
        if (!bici) {
            mensaje = `La bicicleta con codigo ${cuerpo.id} no existe`;
        }
        else {
            const bicicleta = Bicicleta.crearInstancia(cuerpo.id, cuerpo.color, cuerpo.modelo, cuerpo.ubicacion);
            Bicicleta.editar(bicicleta);
            mensaje = `La bicicleta fue actualizada exitosamente`;
        }
        res.status(200).json({
            respuesta: mensaje
        });
    });
}

exports.eliminar_bicicleta = (req, res) => {
    var mensaje = '';
    const indice = req.params.id;
    Bicicleta.encontrarPorCodigo(indice, (error, bici) => {
        if (!bici) {
            mensaje = `La bicicleta con codigo ${indice} no existe`;
            res.status(200).json({
                respuesta: mensaje
            });
        }
        else {
            Bicicleta.eliminarPorCodigo(indice).then(() => {
                mensaje = `La bicicleta fue eliminada exitosamente`;
                res.status(200).json({
                    respuesta: mensaje
                });
            });
        };
    });
}
