require('dotenv').config();
require('newrelic');
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mailTransporter = require('./mailer/mailer')
var indexRouter = require('./routes/index');
var sessionRouter = require('./routes/session');
var usersRouter = require('./routes/users');
var tokenRouter = require('./routes/token');
var bicisRouter = require('./routes/bicicletas');
var bicisAPIRouter = require('./API/bicicletas/routes/bicicletas');
var passport = require('./config/passport');
const session = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);
var app = express();
const jwt = require('jsonwebtoken');
const apiAuthRouter = require('./API/authRouter');
var apiUsuariosRouter = require('./API/usuariosRouterApi');

let store;
if (process.env.NODE_ENV === 'dev') {
  store = new session.MemoryStore;
} else {
  store = new MongoDBStore({
    uri: process.env.MONGO_URI,
    collection: 'sessions'
  });
  store.on('error', function (error) {
    assert.ifError(error);
    assert.ok(false);
  });
}




app.use(session({
  cookie: { maxAge: 240 * 60 * 60 * 100 },
  store: store,
  saveUninitialized: true,
  resave: 'true',
  secret: '***red_bicicletas_cesar***'
}));


//Mongo db
var mongoose = require('mongoose');
//var mongoDB = 'mongodb://localhost/red_bicicletas'
var mongoDB = process.env.MONGO_URI;
mongoose.connect(mongoDB, { useNewUrlParser: true });
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error: '));
/* db.once('open',  () => {
  console.log('Conectado a la bd!');
  done();
}); */

/* //send mail
let enviarCorreo = async () => {
// send mail with defined transport object
let info = await mailTransporter.sendMail({
  from: '"Fred Foo 👻" <titus.champlin@ethereal.email>', // sender address
  to: "titus.champlin@ethereal.email", // list of receivers
  subject: "Hello ✔", // Subject line
  text: "Hello world?", // plain text body
  html: "<b>Hello world?</b>" // html body
});
console.log("Message sent: %s", info.messageId);
}
enviarCorreo(); */
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.set('secretKey', '***red_bicicletas_cesar***');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/', indexRouter);
app.use('/', sessionRouter);
app.use('/usuarios', loggedIn, usersRouter);
app.use('/token', loggedIn, tokenRouter);
app.use('/bicicletas', loggedIn, bicisRouter);
app.use('/api/v1/bicicletas', bicisAPIRouter);
app.use('/api/v1/auth', apiAuthRouter);
app.use('/api/v1/usuarios', apiUsuariosRouter);
app.use('/privacidad', function (req, res) {
  res.sendFile('public/privacy.html')
});
app.use('/validacion', function (req, res) {
  res.sendFile('public/google4eacdb291749ddd3.html');
});

app.get('/auth/google', passport.authenticate('google', {
  scope: [
    //'https://www.googleapis.com/auth/plus.login',
    //'https://www.googleapis.com/auth/plus.profile.emails.read',
    'profile',
    'email'
  ]
}));


app.get('/auth/google/callback', passport.authenticate('google', {
  successRedirect: '/',
  failureRedirect: '/error'
}));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  // render the error page
  res.status(err.status || 500);
  res.render('error');
});



function loggedIn(req, res, next) {
  if (req.user) {
    next();
  }
  else {
    res.redirect('/login');
  }
};


function validarUsuario(req, res, next) {
  jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function (err, decoded) {
    if (err) {
      res.json({
        status: 'error',
        message: err.message,
        data: null
      });
    } else {
      req.body.userId = decoded.id;
      console.log('jwt verify: ', decoded);
      next();
    }
  })
}

module.exports = app;
